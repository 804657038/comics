<?php
declare (strict_types = 1);

namespace app\middleware;
use app\model\AdminLog;
use think\facade\Session;

class Log
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //
        $response = $next($request);

        // 添加中间件执行代码
        return $response;
    }

    public function end(\think\Response $response)
    {
        // 回调行为
        $request = request();

        if($request->isPost()){

            (new AdminLog())->add($response->getContent());
        }
    }

}
