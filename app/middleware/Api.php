<?php
declare (strict_types = 1);

namespace app\middleware;
use think\facade\Session;
use wechat\Login;
use app\model\Set;
use app\model\Member;
class Api
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        //
        if (!Session::has("member_user")) {

            $set =(new Set())->cacheList();
            $wechat = new Login($set["appid"],$set["appsecret"]);
            $url = url('api/login/index',[],true,true)->__toString();
            return json()->data([
                "code"=>-3,
                "message"=>"请先登录",
                "data"=>[],
                "url"=>$wechat->GetLoginUrl($url)
            ]);
        }
        return $next($request);
    }
}
