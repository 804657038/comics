<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/4/21
 * Time: 15:08
 */

namespace app\api\controller;

use app\BaseController;
use think\App;
use app\model\Set;
use think\facade\Session;
use wechat\Login as wechat;
use app\model\Member;
class Login extends BaseController
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    public function login(){
        $member = (new Member)->where("id",1)->find()->toArray();

        Session::set($this->memberkey,$member);
    }
    public function index(){
        $code = $this->request->get("code");
        $set =(new Set())->cacheList();
        $wechat = new wechat($set["appid"],$set["appsecret"]);
        $res = $wechat->oauth($code);
        if(!$res){
            return $this->error($wechat->getResponse()["msg"]);
        }
        $userInfo = $wechat->getResponse()["data"];
        $user = (new Member())->getUser($userInfo);
        Session::set($this->memberkey,$user);
        $this->to($this->request->domain()."/"."lianyou/index.html");
    }
}