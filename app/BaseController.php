<?php
declare (strict_types = 1);

namespace app;

use think\App;
use think\Exception;
use think\exception\HttpException;
use think\exception\ValidateException;
use think\facade\Session;
use think\Validate;
use think\facade\View;
use think\exception\HttpResponseException;
use think\Response;
/**
 * 控制器基础类
 */
abstract class BaseController
{
    /**
     * Request实例
     * @var \think\Request
     */
    protected $request;

    /**
     * 应用实例
     * @var \think\App
     */
    protected $app;

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;

    /**
     * 控制器中间件
     * @var array
     */
    protected $middleware = [];
    public $adminkey = "admin_user";
    public $memberkey = "member_user";
    /**
     * 构造方法
     * @access public
     * @param  App  $app  应用对象
     */
    public function __construct(App $app)
    {
        $this->app     = $app;
        $this->request = $this->app->request;

        // 控制器初始化
        $this->initialize();
    }

    // 初始化
    protected function initialize()
    {}


    /**
     * 验证数据
     * @access protected
     * @param  array        $data     数据
     * @param  string|array $validate 验证器名或者验证规则数组
     * @param  array        $message  提示信息
     * @param  bool         $batch    是否批量验证
     * @return array|string|true
     * @throws ValidateException
     */
    protected function validate(array $data, $validate, array $message = [], bool $batch = false)
    {
        if (is_array($validate)) {
            $v = new Validate();
            $v->rule($validate);
        } else {
            if (strpos($validate, '.')) {
                // 支持场景
                [$validate, $scene] = explode('.', $validate);
            }
            $class = false !== strpos($validate, '\\') ? $validate : $this->app->parseClass('validate', $validate);
            $v     = new $class();
            if (!empty($scene)) {
                $v->scene($scene);
            }
        }

        $v->message($message);

        // 是否批量验证
        if ($batch || $this->batchValidate) {
            $v->batch(true);
        }

        $bool = $v->failException(false)->check($data);

        if(!$bool){
            return $v->getError();
        }
        return false;
    }
    protected function success(string $msg,array $data=[],string $url="") {
        if(empty($data)){
            $data = (object)[];
        }
        return json()->data([
            "code"=>200,
            "message"=>$msg??"获取成功",
            "data"=>$data,
            "url"=>$url
        ]);
    }
    protected function error(string $msg,array $data=[],string $url=""){

        if(!$this->request->isAjax()){
            $multi = app('http')->getName();

            if($multi=="admin"){
                Session::flash("error_msg",$msg);
                Session::flash("error_url",$url);
                $this->to((string)url("index/warning"));
            }else if($multi=="api"){
                return json()->data([
                    "code"=>0,
                    "message"=>$msg,
                    "data"=>$data,
                    "url"=>$url
                ]);
            }


        }else{
            return json()->data([
                "code"=>0,
                "message"=>$msg,
                "data"=>$data,
                "url"=>$url
            ]);
        }


    }


    /**
     * @title 输出模板
     * @param string $template
     * @param array $data
     * @return string
     */
    public function view(string $template,array $data=[]):string{
        return View::fetch($template,$data);
    }

    /**
     * @title 模板赋值
     * @param string $key
     * @param $value
     */
    public function assign(string $key,$value){
        View::assign($key,$value);
    }
    public function to($url){

        throw new HttpResponseException(redirect($url));
    }
    public function utfToGbk($data)
    {

        return mb_convert_encoding($data,'GBK','utf-8');
    }
}
