<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class AdminUser extends Model
{
    //
    public function getRoleNameAttr(){
        $roles = $this->roles;
        $name = (new AdminRoles())->where("id",$roles)->value("name");
        return $name;
    }
}
