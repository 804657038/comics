<?php
declare (strict_types = 1);

namespace app\model;

use app\controller\Common;
use think\facade\Session;
use think\Model;

/**
 * @mixin \think\Model
 */
class AdminLog extends Model
{
    //
    public function getUserNameAttr(){
        $user_id = $this->user_id;
        $name = (new AdminUser())->where("id",$user_id)->value("name");
        return $name;
    }
    public function getActionNameAttr(){
        $action = $this->action;
        $arr = explode("/",$action);
        $purviews = purviews();
        $s1 = $purviews[$arr[0]]["name"];

        $s2 = $purviews[$arr[0]]["child"][$arr[1]];
        return $s1."/".$s2;
    }
    public function add(string $response=""){
        $user = Session::get("admin_user");
        $data  = request()->post();
        $action = request()->action();
        $controller = request()->controller();

        $this->user_id = $user["id"];
        $this->action =strtolower($controller)."/".strtolower($action);

        $this->content = json_encode($data,JSON_UNESCAPED_UNICODE);
        $this->add_time = date("Y-m-d H:i:s");
        $this->response =$response;
        $this->save();
    }
}
