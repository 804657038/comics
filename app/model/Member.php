<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class Member extends Model
{
    //
    public function getUser(array $userInfo):array{
        $member = $this->where("openid",$userInfo["openid"])->field("id,openid,nickname,headimgurl,staff_id,has_share")->find();
        if(empty($member)){
            $data = [
                "openid"=>$userInfo["openid"],
                "nickname"=>$userInfo["nickname"],
                "headimgurl"=>$userInfo["headimgurl"],
                "add_time"=>date("Y-m-d H:i:s"),
            ];
            $id = $this->insertGetId($data);
            $data["id"]=$id;
            $data["staff_id"]="";
            return $data;
        }else{
            return $member->toArray();
        }
    }
}
