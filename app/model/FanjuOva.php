<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class FanjuOva extends Model
{
    //
    //
    public function getStaffAttr(){
        $movie_id = $this->id;
        $lists = (new OvaStaff())->where("movie_id",$movie_id)->select();
        $catarray = (new OvaStaff())->catarray;
        if(!$lists){
            return [];
        }
        foreach ($catarray as $key=>$val){
            $catarray[$key]=[];
        }
        foreach ($lists as $val){
            $catarray[$val["cat"]][]=$val;
        }
        return $catarray;
    }
}
