<?php
declare (strict_types = 1);

namespace app\model;

use think\facade\Cache;
use think\Model;

/**
 * @mixin \think\Model
 */
class Set extends Model
{
    //
    public $cacheKey = "webset";
    public function cacheList():array {
        if(Cache::has($this->cacheKey)){
            return Cache::get($this->cacheKey);
        }else{
            $res = $this->where(true)->column("set_value","set_field");
            Cache::get($this->cacheKey,$res);
            return $res;
        }
    }

    public function clearCache(){
        Cache::delete($this->cacheKey);
    }
}
