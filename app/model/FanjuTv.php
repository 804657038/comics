<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class FanjuTv extends Model
{


    public function getStaffAttr(){
        $tv_id = $this->id;
        $lists = (new FanjuTvStaff())->where("tv_id",$tv_id)->select();
        $catarray = (new FanjuTvStaff())->catarray;
        if(!$lists){
            return [];
        }
        foreach ($catarray as $key=>$val){
            $catarray[$key]=[];
        }
        foreach ($lists as $val){
            $catarray[$val["cat"]][]=$val;
        }
        return $catarray;
    }

    /**
     * @title 获取话数
     * @param $val
     * @return string
     */
    public function getWords1Attr($val){
        if($this->words1_str){
            return $val.$this->words1_str;
        }else{
            return $val;
        }
    }
}
