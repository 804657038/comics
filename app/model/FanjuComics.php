<?php
declare (strict_types = 1);

namespace app\model;

use think\facade\Db;
use think\Model;
use app\model\FanjuTv;
/**
 * @mixin \think\Model
 */
class FanjuComics extends Model
{
    /**
     * @title 获取TV
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getTvAttr(){
        $model = new FanjuTv();

        $work_id = $this->work_id;
        $words1 = $this->words1;
        $comics_rolls = $this->comics_rolls;
        $comics_rolls_file = $this->comics_rolls_file;
        $where = "(";
        $where .= "((comics_words_file1={$words1}) OR (comics_words_file1<={$words1} AND comics_words_file2>={$words1}))";
        if($comics_rolls){
            $where .=" AND ((comics_rolls_1={$comics_rolls}) OR (comics_rolls_1<={$comics_rolls} AND comics_rolls_2>={$comics_rolls}))";
        }
        if($comics_rolls_file){
            $where .=" AND ((comics_rolls_1_file={$comics_rolls_file}) OR (comics_rolls_1_file<={$comics_rolls_file} AND comics_rolls_2_file>={$comics_rolls_file}))";
        }
        $where .= ")";

        $tv = $model
            ->where("work_id",$work_id)
            ->whereIn("fan_cat",[2,1])
            ->whereIn("make_cat",[2,1,4])
            ->where($where)
            ->select();

        return $tv;
    }

}
