<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class FanjuWork extends Model
{
    public $workformnames=[
        1=>"TV",
        2=>"漫画",
        3=>"电影",
        4=>"OVA",
    ];
    //
    public function getCompanyIdsAttr($val){
        return explode(",",$val);
    }
    public function setCompanyIdsAttr($val){
        return implode(",",$val);
    }
    public function getWorkFormAttr($val){
        return explode(",",$val);
    }
    public function setWorkFormAttr($val){
        return implode(",",$val);
    }
    public function getTvStartTimeAttr(){
        $addtime = (new FanjuTv())
            ->where("work_id",$this->id)
            ->where("make_cat",'<>',4)
            ->where("words1",'=',1)
            ->value("add_time");

        if($addtime){
            return date("Y年n月j日",$addtime);
        }else{
            return "无数据";
        }
    }
    public function getComicsStartTimeAttr(){
        $addtime = (new FanjuComics())
            ->where("typec",1)
            ->where("work_id",$this->id)
            ->where("words1",'=',1)
            ->value("add_time");
        if($addtime){

            return date("Y年n月j日",$addtime);

        }else{
            return "无数据";
        }
    }

    /**
     * @title 获取TV连载话数
     * @return mixed
     */
    public function getTvEndWordsAttr(){
        $words1 = (new FanjuTv())->where("work_id",$this->id)->order("words1 desc")->value("words1");
        return $words1??0;
    }
    /**
     * @title 获取漫画连载话数
     * @return mixed
     */
    public function getComicsEndWordsAttr(){
        $words1 = (new FanjuComics())->where("work_id",$this->id)->order("words1 desc")->value("words1");
        return $words1??0;
    }
    public function getCompanysAttr(){
        $lists = (new Company())->whereIn("id",$this->company_ids)->select();
        return $lists;
    }
}
