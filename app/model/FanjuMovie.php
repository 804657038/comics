<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin \think\Model
 */
class FanjuMovie extends Model
{
    //
    //
    public function getStaffAttr(){
        $movie_id = $this->id;
        $lists = (new MovieStaff())->where("movie_id",$movie_id)->select();
        $catarray = (new MovieStaff())->catarray;
        if(!$lists){
            return [];
        }
        foreach ($catarray as $key=>$val){
            $catarray[$key]=[];
        }
        foreach ($lists as $val){
            $catarray[$val["cat"]][]=$val;
        }
        return $catarray;
    }
}
