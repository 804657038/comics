<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/4/22
 * Time: 15:47
 */

namespace app\admin\controller;

use think\App;
use app\model\Member as model;
class Member extends Common
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @title 用户列表
     * @return mixed
     * @throws \think\db\exception\DbException
     */
    public function index(){
        $nickname = $this->request->get("nickname",null);
        $where = [];
        if($nickname){
            $where[]=["nickname","LIKE","%".$nickname."%"];
        }
        $model = new model();
        $lists = $model->where($where)->paginate([
            "list_rows"=>10,
            "query"=>[
                "nickname"=>$nickname,
            ]
        ]);
        return $this->view("index",[
            "lists"=>$lists
        ]);
    }
}