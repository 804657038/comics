<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/4/16
 * Time: 17:26
 */

namespace app\admin\controller;


use think\App;
use think\facade\Db;
use app\model\Set as model;
class Set extends Common
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function index(){
        if($this->request->isPost()){
            $data = $this->request->post();
            foreach ($data as $key=>$item){
                Db::name("set")->where("set_field",$key)->update([
                    "set_value"=>$item
                ]);
            }
            (new model())->clearCache();
            return $this->success(lang("edit").lang("success"));
        }
        $lists = Db::name('set')->select();

        return $this->view("",[
            "lists"=>$lists
        ]);
    }

    public function upload(){
        $request = $this->request;

        $files = $request->file();
        try {
            validate(['file'=>'filesize:204800|fileExt:jpg,png,jpeg'],[
                'file.filesize'=>'头像大小不能超过200K',
            ])->check($files);
            $savename = [];
            foreach($files as $file) {
                $savename[] = \think\facade\Filesystem::putFile( "images", $file);
            }
            $string = str_replace("\\","/",$savename[0]);
            return $this->success("",[
                "path"=>$string,
                "url"=>formatImg($string)
            ]);
        } catch (\think\exception\ValidateException $e) {
            return $this->error($e->getMessage());
        }
    }
}