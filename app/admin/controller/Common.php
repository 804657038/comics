<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/4/7
 * Time: 11:03
 */

namespace app\admin\controller;

use app\BaseController;
use app\model\AdminRoles;
use app\model\AdminUser;
use think\App;
use think\facade\Cache;
use think\facade\Session;
use app\middleware\Log;
class Common extends BaseController
{
    public $admin;
    protected $middleware = [Log::class];
    public function __construct(App $app)
    {
        parent::__construct($app);

        if(!Session::has($this->adminkey)){

            if($this->request->isAjax()){
                return $this->error(lang('please.login'),[],url("login/index"));
            }else{
                $this->to((string)url("login/index"));
            }
        }else{

            $this->admin = Session::get($this->adminkey);
        }
        $this->userpurviews();

    }
    public function userpurviews(){

        if($this->admin["roles"]!=1){
            //查看账号是否被封
            $display = (new AdminUser())->where("id",$this->admin["id"])->value("display");

            if((int)$display==0){
                if($this->request->isAjax()){
                    echo json_encode($this->error(lang('you.username.blockade'),[],url("login/index")));
                    exit;
                }else{
                    return $this->error(lang('you.username.blockade'),[],url("login/index"));
                }

            }
            $purviews = (new AdminRoles())->where("id",$this->admin["roles"])->value("purview");
            $purviewArr = explode(",",$purviews);

            $action = request()->action();
            $controller = request()->controller();

            $s = strtolower($controller)."/".strtolower($action);

            if(strtolower($controller)!="index"){

                if(!in_array($s,$purviewArr)){
                    if($this->request->isAjax()){
                        echo json_encode($this->error(lang('not').lang("action").lang("purview"))->getData());
                        exit;
                    }else{

                        return $this->error(lang('not').lang("action").lang("purview"));
                    }


                }
            }

        }
    }
    public function menus(){
        $data = menus();
        if($this->admin["roles"]==1){
            return $data;
        }else{
            $purviews = (new AdminRoles())->where("id",$this->admin["roles"])->value("purview");
            $purviewArr = explode(",",$purviews);

            foreach ($data as $key=>$val){
                if(!in_array($val["path"],$purviewArr)){
                    unset($data[$key]);
                }else{
                    foreach ($val["childs"] as $k=>$v){
                        if(!in_array($v["path"],$purviewArr)){
                            unset($data[$key]["childs"][$k]);
                        }
                    }
                }
            }
            return $data;
        }
    }
    public function purviews(){

        return purviews();
    }

}