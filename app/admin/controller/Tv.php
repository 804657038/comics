<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/8/22
 * Time: 15:48
 */

namespace app\admin\controller;

use app\model\FanjuTvStaff;
use app\model\FanjuTv;
use app\model\FanjuWork;
use app\model\TvSeiyuu;
use app\model\Personnel;
use app\model\Channel;
use think\facade\Db;

class Tv extends Common
{
    public function index(){

    }

    /**
     * @title 新增话数
     * @return $this
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add(){
        $model = new FanjuTv();
        $work_id = $this->request->post("work_id");
        if(!$work_id){
            return $this->error("作品ID不能为空");
        }
        $tvlist = (new FanjuTv())
            ->field("words1")
            ->where("work_id",$work_id)
            ->limit(10)
            ->order("words1 desc")
            ->select();
        $arr = [];
        foreach ($tvlist as $val){
            $arr[]=intval($val["words1"]);
        }
        sort($arr);
        if(count($arr)>=1){
            $words1 = $arr[count($arr)-1];

            for($i=0;$i<count($arr);$i++){
                if(isset($arr[$i+1])){
                    if($arr[$i]+1!=$arr[$i+1]){
                        $words1 = $arr[$i];
                    }
                }
            }
        }else{
            $words1 = 0;
        }

        $id = $model->insertGetId([
            "work_id"=>$work_id,
            "words1"=>$words1+1
        ]);
        return $this->success("",[
            "id"=>$id,
            "words1"=>$words1+1
        ]);

    }

    /**
     * @title 提交
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function post(){
        if($this->request->isPost()){
            $staffModel = new FanjuTvStaff();
            $tvModel = new FanjuTv();
            $seiYuuModel = new TvSeiyuu();
            $tvInfo = [
                'fan_cat'=>$this->request->post('fan_cat'),
                'special_cat'=>$this->request->post('special_cat'),
                'make_cat'=>$this->request->post('make_cat'),
                'replay_make_cat'=>$this->request->post('replay_make_cat'),
                'quarter'=>$this->request->post('quarter'),
                'words1'=>$this->request->post('words1'),
                'words2'=>$this->request->post('words2'),
                'title'=>$this->request->post('title'),
                'title_jp'=>$this->request->post('title_jp'),
                'add_time'=>$this->request->post('add_time'),
                'tvstation'=>$this->request->post('tvstation'),
                'tvlenth'=>$this->request->post('tvlenth'),
                'tvother'=>$this->request->post('tvother'),
                'comics_words_file1'=>$this->request->post('comics_words_file1'),
                'comics_words_file2'=>$this->request->post('comics_words_file2'),
                'comics_rolls_1'=>$this->request->post('comics_rolls_1'),
                'comics_rolls_1_file'=>$this->request->post('comics_rolls_1_file'),
                'comics_rolls_2'=>$this->request->post('comics_rolls_2'),
                'comics_rolls_2_file'=>$this->request->post('comics_rolls_2_file'),
                'op_code'=>$this->request->post('op_code'),
                'op_title'=>$this->request->post('op_title'),
                'ed_code'=>$this->request->post('ed_code'),
                'ed_title'=>$this->request->post('ed_title'),
                "display"=>$this->request->post("display",0),

            ];
            $tvInfo["add_time"] =trim(str_replace(["年","月","日"],'-',$tvInfo["add_time"]),"-");
            $tvInfo["add_time"] = strtotime($tvInfo["add_time"]);
            $tvstation = $this->request->post("tvstation",null);

            if($tvstation){
                $tvInfo["tvstation"] = implode(",",$tvstation);
            }else{
                $tvInfo["tvstation"] = null;
            }

            if($tvInfo["display"]==1){
                $tvInfo["display"]=0;
            }else{
                $tvInfo["display"]=1;
            }
            $id = $this->request->post("id",5);

            $words1 = findNum($tvInfo["words1"]);

            $words1_str = str_replace($words1,"",$tvInfo["words1"]);
            $tvInfo["words1"] = $words1;
            $tvInfo["words1_str"] = $words1_str;
            //查看话数有无重复的
            if($tvInfo["make_cat"]!=4){
                $work_id = $this->request->post("work_id");
                $item = (new FanjuTv())->where("work_id",$work_id)
                    ->where("words1",$words1)
                    ->where("id",'<>',$id)
                    ->where("fan_cat",'=',$tvInfo["fan_cat"])
                    ->find();
                if($item){
                    return $this->error("{$words1}话已存在,如要继续，请选择制作类型重播");
                }
            }

            $tvModel->where("id",$id)->update($tvInfo);
            $staff = $this->request->post("staff");
            $staffArray = [];

            $item = (new FanjuTv())->find($id);

            foreach ($staff as $val){
                if($val['uname']){
                    $val["tv_id"] = $id;
                    (new Personnel())->add($val['uname'],2,$val["uname_jp"],0,$item["work_id"]);
                    array_push($staffArray,$val);
                }
            }

            $staffModel->where("tv_id",$id)->delete();
            $staffModel->insertAll($staffArray);

            $seiyuu = $this->request->post("seiyuu");
            foreach ($seiyuu as &$val){
                $val["tv_id"] = $id;
                if($val["cat"]==1){
                    if($val['seiyuu_zh']){
                        (new Personnel())->add($val['seiyuu_zh'],3,$val["seiyuu_jp"],0,$item["work_id"]);
                    }
                }
            }
            unset($val);
            $seiYuuModel->where("tv_id",$id)->delete();
            $seiYuuModel->insertAll($seiyuu);
            return $this->success("提交成功");
        }else{
            $isAjax = $this->request->isAjax();
            if($isAjax){
                $resData = [];
                $id = $this->request->get("id");

                $fieldName = $this->request->get("fieldName");
                $work_id = $this->request->get("work_id");
                $quarter = $this->request->get("quarter");
                $fan_cat = $this->request->get("fan_cat");
                $special_cat = $this->request->get("special_cat");
                $make_cat = $this->request->get("make_cat");
                $fieldValue = $this->request->get("fieldValue");
                $fieldValue = preg_replace('/\D/s', '', $fieldValue);
                $where = [];
                $where[] = ["id","<>",$id];
                $where[] = ["work_id","=",$work_id];
                $where[] = ["fan_cat","=",$fan_cat];
                if($fan_cat==2){
                    $where[] =[$fieldName,'=',$fieldValue];
                }else if($fan_cat==1){
                    $where[] =[$fieldName,'=',$fieldValue];
                    $where[] =["quarter",'=',$quarter];
                }else if($fan_cat==3){
                    $where[] =[$fieldName,'=',$fieldValue];
                }

                $item = (new FanjuTv())->where($where)->find();

                if($item){
                    $staff = $item->staff;
                    $item = $item->toArray();
                    $seiyuulists = (new TvSeiyuu())->where("tv_id",$item["id"])->select();
                    $item["staff"] = $staff;
                    $item["seiyuulists"] = $seiyuulists;
                    $resData = $item;
                }else{
                    if($make_cat==4){
                        return $this->error("改话未首更,请手动退出重播");
                    }
                }
                return $this->success("",$resData);
            }
            $id = $this->request->get("id");
            $item = (new FanjuTv())->find($id);
            $work = (new FanjuWork())->find($item["work_id"]);
            $catarray = (new FanjuTvStaff())->catarray;
            $staff = $item->staff;
            $seiYuuModel = new TvSeiyuu();
            $seiyuulists = $seiYuuModel->where("tv_id",$id)->select();
            if($item["tvstation"]){
                $channel = (new Channel())->where("id",$item["tvstation"])->select();
            }else{
                $channel = [];
            }

            return $this->view('',[
                "work"=>$work,
                "item"=>$item,
                "catarray"=>$catarray,
                "staff"=>$staff,
                "seiyuulists"=>$seiyuulists,
                "channel"=>$channel,
            ]);
        }
    }

    /**
     * @title 删除内容
     * @return $this
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new FanjuTv();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
            (new FanjuTvStaff())->whereIn("tv_id",$id)->delete();
            (new TvSeiyuu())->whereIn("tv_id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
            (new FanjuTvStaff())->where("tv_id",$id)->delete();
            (new TvSeiyuu())->where("tv_id",$id)->delete();
        }

        return $this->success("删除成功");
    }

    /**
     * @title 声优查看
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function seiyuu(){
        $id = $this->request->get("id");
        $tv = (new FanjuTv())->where("id",$id)->find();
        $lists = (new TvSeiyuu())->where("tv_id",$id)->select();

        return $this->view("",[
            'tv'=>$tv,
            'lists'=>$lists
        ]);

    }



}