<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/4/7
 * Time: 18:12
 */

namespace app\admin\controller;


use app\model\AdminLog;
use app\model\AdminRoles;
use app\model\AdminUser;
use think\App;
use think\facade\Db;
use think\helper\Str;
class User extends Common
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @title 账号列表
     * @return string
     * @throws \think\db\exception\DbException
     */
    public function index(){
        $lists = (new AdminUser())->append(["role_name"])->paginate(10);
        return $this->view("",[
            "lists"=>$lists
        ]);
    }

    /**
     * @title 添加账号
     * @return $this|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function add(){
        $id = $this->request->param("id",null);
        if($this->request->isPost()){
            $data = $this->request->post();

            $check = $this->validate($data,[
                'username|'.lang("username")=>'require',
                'password|'.lang("pass")=>'require|alphaNum|length:6,18',
                'pass|'.lang('confirm').lang('pass')=>'require|confirm:password',
                'name|'.lang('name')=>'require',
                'display|'.lang('status')=>'require|in:1,0',
                'roles|'.lang('roles')=>'require'
            ],[
                'username.require'=>lang("username").lang("require"),
                'password.require'=>lang("pass").lang("require"),
                'password.alphaNum'=>lang("pass").lang("alphaNum"),
                'password.length'=>lang("pass").lang("length").": 6-18",
                'pass.require'=>lang('confirm').lang('pass').lang("require"),
                'pass.confirm'=>lang('confirm').lang('pass').lang("error"),
                'name.require'=>lang('name').lang('require'),
                'display.require'=>lang('status').lang('require'),
                'display.in'=>lang('status').lang('in').":1,0",
                'roles.require'=>lang('roles').lang('require'),
            ]);
            if($check){
                return $this->error($check);
            }
            $hasusername = (new AdminUser())->where("username",$data["username"])->value("id");
            if($hasusername){
                return $this->error(lang("username").lang("existed").",".lang('Please.replace'));
            }
            $salt = Str::random(5);
            (new AdminUser())->insert([
                "username"=>$data["username"],
                "salt"=>$salt,
                "pass"=>md5pass($data["password"],$salt),
                "roles"=>$data["roles"],
                "name"=>$data["name"],
                "display"=>$data["display"],
                "phone"=>isset($data["phone"])?$data["phone"]:"",
                "add_time"=>date("Y-m-d H:i:s")
            ]);
            return $this->success(lang("add").lang("success"));
        }else{
            $roles = (new AdminRoles())->field("id,name")->select();
            return $this->view("post",[
                "id"=>$id,
                "roles"=>$roles
            ]);
        }

    }

    /**
     * @title 员工详情
     */
    public function edit(){
        $id = $this->request->param("id");

        if($this->request->isPost()){
            $data = $this->request->post();

            $check = $this->validate($data,[
                'name|'.lang("name")=>'require',
                'roles|'.lang("roles")=>'require'
            ],[
                'name.require'=>lang("name").lang("require"),
                'roles.require'=>lang("roles").lang("require"),
            ]);
            if($check){
                return $this->error($check);
            }
            (new AdminUser())->where("id",$id)->update([
                "name"=>$data["name"],
                "roles"=>$data["roles"],
                "phone"=>isset($data["phone"])?$data["phone"]:"",
            ]);
            return $this->success(lang('edit').lang("success"));
        }
        $user = (new AdminUser())->find($id);
        $roles = (new AdminRoles())->field("id,name")->select();
        return $this->view("",[
            "user"=>$user,
            "roles"=>$roles
        ]);
    }

    /**
     * @title 删除员工
     */
    public function del(){
        $id = $this->request->post("id");
        $user = (new AdminUser())->where("id",$id)->find();
        if($user->roles==1){
            $num = (new AdminUser())->where("roles",1)->count();
            if($num<=1){
                return $this->error(lang('user_del_1'));
            }
        }
        (new AdminUser())->where("id",$id)->delete();
        return $this->success(lang("del"),lang("success"));
    }
    /**
     * @title 修改账号状态
     * @return $this
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function display(){
        $id = $this->request->post("id");
        $display = $this->request->post("display");
        $user = (new AdminUser())->where("id",$id)->find();
        if($user->roles == 1){
            return $this->error(lang('user_status_1'));
        }
        $user->display = $display;
//        $user->save();
        return $this->success(lang('edit').lang('success'));
    }

    /**
     * @title 修改密码
     */
    public function pass(){
        $id = $this->request->param("id");
        $user = (new AdminUser())->find($id);
        if($this->request->isPost()){
            if(!$user){
                return $this->error(lang('username').lang('does.not.exist'));
            }
            $data = $this->request->post();

            $check = $this->validate($data,[
                'password|'.lang('pass')=>'require|alphaNum|length:6,18',
                'pass|'.lang('confirm').lang('pass')=>'require|confirm:password',
            ],[
                'password.require'=>lang("pass").lang("require"),
                'password.alphaNum'=>lang("pass").lang("alphaNum"),
                'password.length'=>lang("pass").lang("length").": 6-18",
                'pass.require'=>lang('confirm').lang('pass').lang("require"),
                'pass.confirm'=>lang('confirm').lang('pass').lang("error"),
            ]);
            if($check){
                return $this->error($check);
            }
            $salt = $user->salt;
            $user->pass = md5pass($data["password"],$salt);
            $user->save();
            return $this->success(lang('edit').lang('success'));
        }
        return $this->view("",[
            "user"=>$user
        ]);
    }

    /**
     * @title 角色管理
     */
    public function roles(){

        $lists = (new AdminRoles())->paginate(10);

        return $this->view("",[
            "lists"=>$lists
        ]);
    }

    /**
     * @title 添加角色
     * @return $this|string
     * @throws \Exception
     */
    public function rolesadd(){
        if($this->request->isPost()){
            $name = $this->request->post("name","");
            $desc = $this->request->post("desc","");
            $roles = $this->request->post("roles",[]);
            if(!$name){
                return $this->error(lang('roles').lang('name').lang('require'));
            }

            if(!is_array($roles)){
                return $this->error(lang('purview').lang('error'));
            }
            (new AdminRoles())->insert([
                "name"=>$name,
                "desc"=>$desc,
                "purview"=>implode(",",$roles),
            ]);
            return $this->success(lang("add").lang("success"));
        }else{
            $purviews = $this->purviews();
            return $this->view("",[
                "purviews"=>$purviews
            ]);
        }
    }


    public function rolesedit(){
        if($this->request->isPost()){
            $id = $this->request->post("id","");
            $name = $this->request->post("name","");
            $desc = $this->request->post("desc","");
            $roles = $this->request->post("roles",[]);
            if(!$name){
                return $this->error(lang('roles').lang('name').lang('require'));
            }

            if(!is_array($roles)){
                return $this->error(lang('purview').lang('error'));
            }

            $r= (new AdminRoles())->where("id",$id)->update([
                "name"=>$name,
                "desc"=>$desc,
                "purview"=>implode(",",$roles),
            ]);

            return $this->success(lang("edit").lang("success"));
        }else{
            $id = $this->request->get("id");
            $item =  (new AdminRoles())->where("id",$id)->find();

            $purviews = $this->purviews();
            $roles = explode(",",$item["purview"]);
            return $this->view("",[
                "purviews"=>$purviews,
                "item"=>$item,
                "roles"=>$roles
            ]);
        }
    }

    /**
     * @title 删除角色
     * @return $this
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function rolesdel(){
        $id = $this->request->post("id","");
        $item = (new AdminRoles())->where("id",$id)->find();
        if($item["id"]==1){
            return $this->error(lang('Super.administrator.not.del'));
        }
        if(!$item){
            return $this->error(lang('roles').lang('does.not.exist'));
        }
        (new AdminRoles())->where("id",$id)->delete();
        return $this->success(lang('del').lang('success'));
    }

    /**
     * @title 操作日志
     * @return string
     * @throws \think\db\exception\DbException
     */
    public function log(){
        $model = new AdminLog();
        $lists=$model->order("id desc")->append(["user_name","action_name"])->paginate();
        return $this->view("",[
            "lists"=>$lists
        ]);
    }
    public function logdel(){

        Db::name('admin_log')->delete(true);

        return $this->success(lang('empty').lang('success'));
    }
}