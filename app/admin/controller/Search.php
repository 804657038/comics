<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/9/23
 * Time: 15:49
 */

namespace app\admin\controller;

use app\model\FanjuWork;
use think\facade\Db;
use app\model\Personnel;
use app\model\FanjuTvStaff;
use app\model\OvaStaff;
use app\model\MovieStaff;
use app\model\Company;
class Search extends Common
{
    /**
     * @title 总搜索
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(){
        $keyword = $this->request->get("keyword",null);
        $work_id = $this->request->get("work_id",null);
        $search_cat = $this->request->get("search_cat",0);
        /**如果search_cat == 1,是作品名搜索，其他都是人名搜索，人名搜索包括制作人和声优**/
        $lists = null;
        $datas = null;
        $company = false;
        $author = false;
        if($search_cat==1){
            if($keyword || $work_id){
                $model = new FanjuWork();
                $model = $model->where("display",1);
                if($work_id){
                    $model = $model
                        ->whereIn("id",$work_id);
                }else{
                    $model = $model
                        ->where("title","LIKE","%".$keyword."%")
                        ->whereOr("title_jp","LIKE","%".$keyword."%");
                }

                $lists = $model->select();
            }
        }else{
            if($keyword || $company){


                $model = new Personnel();
                if($company){
                    $company_title = (new Company())->where("id",$company)->value("title_cn");
                    $this->assign("company_title",$company_title);
                    $keyword = null;
                    $model = $model->where("company_id",$company);

                }else{
                    $model = $model->where("uname",'LIKE','%'.$keyword.'%');
                }
                $lists = $model
                    ->select();


                $tvcatearr = (new FanjuTvStaff())->catarray;
                $moviecatearr = (new MovieStaff())->catarray;
                $Ovacatearr = (new OvaStaff())->catarray;
                $datas["seiyuu"]=[];
                $datas["staff"]=[];
                foreach ($lists as $val){
                    if($val["idcard"]==1){
                        $author = true;
                    }
                    //查看负责的tv
                    $tvlist = (new FanjuTvStaff())
                        ->where("uname",$val["uname"])
                        ->field("cat")
                        ->select();
                    $val["tvnum"] = count($tvlist);
                    $jobs=[];
                    foreach ($tvlist as $v){
                        if(!in_array($tvcatearr[$v["cat"]]['zh'],$jobs)){
                            $jobs[]= $tvcatearr[$v["cat"]]['zh'];

                        }
                    }
                    $movielist = (new MovieStaff())->where("username",$val["uname"])->field("cat")->select();
                    $val["movienum"] = count($movielist);
                    foreach ($movielist as $v){
                        if(!in_array($moviecatearr[$v["cat"]]['zh'],$jobs)){
                            $jobs[]= $moviecatearr[$v["cat"]]['zh'];
                        }
                    }
                    $ovalist = (new OvaStaff())->where("username",$val["uname"])->field("cat")->select();
                    $val["ovanum"] = count($ovalist);
                    foreach ($ovalist as $v){
                        if(!in_array($Ovacatearr[$v["cat"]]['zh'],$jobs)){
                            $jobs[]= $Ovacatearr[$v["cat"]]['zh'];
                        }
                    }

                    $val["jobs"] = implode("、",$jobs);
                    //查看所属公司
                    if($val["company_id"]){
                        $val["company"]=(new Company())->where("id",$val["company_id"])->value("title_cn");
                    }else{
                        $val["company"]="/";
                    }
                    if($val["idcard"]==3){
                        $datas["seiyuu"][]=$val;
                    }else{
                        $datas["staff"][]=$val;
                    }
                }
            }
        }
        return $this->view("",[
            'keyword'=>$keyword,
            'search_cat'=>$search_cat,
            'lists'=>$lists,
            'datas'=>$datas,
            'company'=>$company,
            'author'=>$author,
        ]);
    }

    /**
     * @title 作品名搜索
     * @return string
     * @throws \think\db\exception\DbException
     */
    public function work(){
        $lists = null;
        $keyword = $this->request->get("keyword",null);
        $work_id = $this->request->get("work_id",null);
        if($keyword || $work_id){
            $model = new FanjuWork();
            $model = $model->where("display",1);
            if($work_id){
                $model = $model
                    ->whereIn("id",$work_id);
            }else{
                $model = $model
                    ->where("title","LIKE","%".$keyword."%")
                    ->whereOr("title_jp","LIKE","%".$keyword."%");
            }

            $lists = $model
                ->paginate([
                    "query"=>[
                        "keyword"=>$keyword,
                        "work_id"=>$work_id,
                    ]
                ]);
        }
        return $this->view("",[
            "lists"=>$lists
        ]);

    }
    public function man(){
        $lists = null;
        $datas = null;
        $keyword = $this->request->get("keyword",null);
        $company = $this->request->get("company",null);
        $author = false;

        if($keyword || $company){


            $model = new Personnel();
            if($company){
                $company_title = (new Company())->where("id",$company)->value("title_cn");
                $this->assign("company_title",$company_title);
                $keyword = null;
                $model = $model->where("company_id",$company);

            }else{
                $model = $model->where("uname",'LIKE','%'.$keyword.'%');
            }
            $lists = $model
                ->whereIn("idcard",[1,2])
                ->paginate([
                "query"=>[
                    "keyword"=>$keyword
                ]
            ]);
            $datas = $lists->toArray()["data"];

            $tvcatearr = (new FanjuTvStaff())->catarray;
            $moviecatearr = (new MovieStaff())->catarray;
            $Ovacatearr = (new OvaStaff())->catarray;
            foreach ($datas as &$val){
                if($val["idcard"]==1){
                    $author = true;
                }
                //查看负责的tv
                $tvlist = (new FanjuTvStaff())->where("uname",$val["uname"])->field("cat")->select();
                $val["tvnum"] = count($tvlist);
                $jobs=[];
                foreach ($tvlist as $v){
                    if(!in_array($tvcatearr[$v["cat"]]['zh'],$jobs)){
                        $jobs[]= $tvcatearr[$v["cat"]]['zh'];

                    }
                }
                $movielist = (new MovieStaff())->where("username",$val["uname"])->field("cat")->select();
                $val["movienum"] = count($movielist);
                foreach ($movielist as $v){
                    if(!in_array($moviecatearr[$v["cat"]]['zh'],$jobs)){
                        $jobs[]= $moviecatearr[$v["cat"]]['zh'];
                    }
                }
                $ovalist = (new OvaStaff())->where("username",$val["uname"])->field("cat")->select();
                $val["ovanum"] = count($ovalist);
                foreach ($ovalist as $v){
                    if(!in_array($Ovacatearr[$v["cat"]]['zh'],$jobs)){
                        $jobs[]= $Ovacatearr[$v["cat"]]['zh'];
                    }
                }

                $val["jobs"] = implode("、",$jobs);
                //查看所属公司
                if($val["company_id"]){
                    $val["company"]=(new Company())->where("id",$val["company_id"])->value("title_cn");
                }else{
                    $val["company"]="/";
                }
            }
            unset($val);
        }
        return $this->view("",[
            "lists"=>$lists,
            "datas"=>$datas,
            "author"=>$author,
            "keyword"=>$keyword,
            "company"=>$company,
        ]);
    }
}