<?php
namespace app\admin\controller;

use think\App;
use think\facade\Cache;
use think\facade\Db;
use think\facade\Session;
use app\model\AdminLog;
class Index extends Common
{
    public function __construct(App $app)
    {

        parent::__construct($app);

    }

    public function index()
    {
        $lang = $this->request->get("lang",'zh');
        Cache::set("lang",$lang,0);
        $menus = $this->menus();
        return $this->view("",[
            "user"=>$this->admin,
            "menus"=>$menus
        ]);
    }
    public function index2(){
        $lang = $this->request->get("lang",'zh');
        Cache::set("lang",$lang,0);
        $menus = $this->menus();
        $item = $menus[0];

        if(count($item['childs'])>=1){
            $url = url($item['childs'][0]["path"]);

        }else{
            $url = url($item["path"]);
        }
        return $this->view("index/index1",[
            "user"=>$this->admin,
            "menus"=>$menus,
            "url"=>$url
        ]);
    }

    public function welcome(){
        return $this->view("welcome",[
            "user"=>$this->admin,
        ]);
    }

    public function logout(){

        Session::delete($this->adminkey);

        $this->to((string)url("login/index"));
    }
    public function warning(){
        $msg = Session::get("error_msg","未知错误");
        $url = Session::get("error_url","");

        return $this->view("",[
            "msg"=>$msg,
            "url"=>$url,
        ]);
    }

}
