<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/9/22
 * Time: 16:44
 */

namespace app\admin\controller;

use app\model\FanjuOva;
use app\model\OvaSeiyuu;
use app\model\OvaStaff;
use app\model\FanjuWork;
use app\model\Personnel;
class Ova extends Common
{
    /**
     * @title 新增条目
     * @return $this
     */
    public function add(){
        $model = new FanjuOva();
        $work_id = $this->request->post("work_id");
        if(!$work_id){
            return $this->error("作品ID不能为空");
        }
        $words1 = (new FanjuOva())
            ->where("work_id",$work_id)
            ->order("words1 desc")
            ->count();

        $id = $model->insertGetId([
            "work_id"=>$work_id,
            "words1"=>$words1+1
        ]);
        return $this->success("",[
            "id"=>$id,
            "words1"=>$words1+1
        ]);

    }

    /**
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function post(){
        if($this->request->isPost()){
            $staffModel = new OvaStaff();
            $movieModel = new FanjuOva();
            $seiYuuModel = new OvaSeiyuu();
            $movi1Info=[
                "words1"=>$this->request->post("words1"),
                "cat"=>$this->request->post("cat"),
                "title"=>$this->request->post("title"),
                "title_jp"=>$this->request->post("title_jp"),
                "add_time"=>$this->request->post("add_time"),
                "time_lenth"=>$this->request->post("time_lenth"),
                "song_man"=>$this->request->post("song_man"),
                "song_title"=>$this->request->post("song_title"),
                "publisher"=>$this->request->post("publisher"),
                "display"=>$this->request->post("display",0),
            ];
            $movi1Info["add_time"] =trim(str_replace(["年","月","日"],'-',$movi1Info["add_time"]),"-");
            $movi1Info["add_time"] = strtotime($movi1Info["add_time"]);
            if($movi1Info["display"]==1){
                $movi1Info["display"]=0;
            }else{
                $movi1Info["display"]=1;
            }
            $id = $this->request->post("id",5);
            $movieModel->where("id",$id)->update($movi1Info);
            $staff = $this->request->post("staff");
            $staffArray = [];
            $item = (new FanjuOva())->find($id);
            foreach ($staff as $val){
                if($val['username']){
                    $val["movie_id"] = $id;
                    if($val["cat"]==1){
                        (new Personnel())->add($val['username'],1,$val["username_jp"],0,$item["work_id"]);

                    }else{
                        (new Personnel())->add($val['username'],2,$val["username_jp"],0,$item["work_id"]);

                    }
                    array_push($staffArray,$val);
                }

            }

            $staffModel->where("movie_id",$id)->delete();
            $staffModel->insertAll($staffArray);

            $seiyuu = $this->request->post("seiyuu");
            foreach ($seiyuu as &$val){
                $val["movie_id"] = $id;
                if($val["cat"]==1){
                    if($val['seiyuu_zh']){
                        (new Personnel())->add($val['seiyuu_zh'],3,$val["seiyuu_jp"],0,$item["work_id"]);
                    }
                }
            }
            unset($val);
            $seiYuuModel->where("movie_id",$id)->delete();
            $seiYuuModel->insertAll($seiyuu);

            return $this->success("提交成功");
        }else{
            $id = $this->request->get("id");
            $item = (new FanjuOva())->find($id);
            $work = (new FanjuWork())->find($item["work_id"]);
            $catarray = (new OvaStaff())->catarray;
            $staff = $item->staff;
            $seiYuuModel = new OvaSeiyuu();
            $seiyuulists = $seiYuuModel->where("movie_id",$id)->select();
            return $this->view('',[
                "work"=>$work,
                "item"=>$item,
                "catarray"=>$catarray,
                "staff"=>$staff,
                "seiyuulists"=>$seiyuulists,
            ]);
        }
    }

    /**
     * @title 删除内容
     * @return $this
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new FanjuOva();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
            (new OvaSeiyuu())->whereIn("movie_id",$id)->delete();
            (new OvaStaff())->whereIn("movie_id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
            (new OvaSeiyuu())->where("movie_id",$id)->delete();
            (new OvaStaff())->where("movie_id",$id)->delete();
        }

        return $this->success("删除成功");
    }
    /**
     * @title 声优查看
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function seiyuu(){
        $id = $this->request->get("id");
        $movie = (new FanjuOva())->where("id",$id)->find();
        $lists = (new OvaSeiyuu())->where("movie_id",$id)->select();

        return $this->view("",[
            'movie'=>$movie,
            'lists'=>$lists
        ]);

    }
}