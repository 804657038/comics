<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/9/11
 * Time: 9:26
 */

namespace app\admin\controller;

use app\model\FanjuWork;
use app\model\FanjuComics;
use think\facade\Db;

class Comics extends Common
{

    public function add(){
        $work_id = $this->request->post("work_id");
        if(!$work_id){
            return $this->error("作品ID不能为空");
        }
        $model = new FanjuComics();
        $words1 = $model
            ->where("work_id",$work_id)
            ->order("words1 desc")
            ->count();
        $id = $model->insertGetId([
            "work_id"=>$work_id,
            "words1"=>$words1+1
        ]);
        return $this->success("",[
            "id"=>$id,
            "words1"=>$words1+1
        ]);
    }

    /**
     * @title 提交
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function post(){
        if($this->request->isPost()){
            $model = new FanjuComics();
            $data = $this->request->post();
            $display = $this->request->post("display",0);
            unset($data["dbname"]);
            $data["add_time"] =trim(str_replace(["年","月","日"],'-',$data["add_time"]),"-");
            $data["release_date"] =trim(str_replace(["年","月","日"],'-',$data["release_date"]),"-");
            $data["release_date"] = strtotime($data["release_date"]);
            $data["add_time"] = strtotime($data["add_time"]);
            $data["display"] = $display;
            if($data["display"]==1){
                $data["display"]=0;
            }else{
                $data["display"]=1;
            }
            $model->where("id",$data["id"])->save($data);
            return $this->success("保存成功");
        }else{
            $isAjax = $this->request->isAjax();
            if($isAjax){
                $work_id = $this->request->get("work_id");
                $comics_rolls = $this->request->get("comics_rolls");
                $model = new FanjuComics();

                $item = $model->where("work_id",$work_id)->where("comics_rolls",$comics_rolls)
                    ->find();
                if($item){
                    $item = $item->toArray();
                    $item["release_date"] = date("Y年n月j日",$item["release_date"]??time());
                    return $this->success("",$item); $item["release_date"] = date("Y年n月j日",$item["release_date"]??time());

                }else{
                    return $this->success("",[]);
                }

            }
            $id = $this->request->get("id");
            $item = (new FanjuComics())->find($id);
            $work = (new FanjuWork())->find($item["work_id"]);
            $item["release_date"] = date("Y年n月j日",$item["release_date"]??time());
            $item["add_time"] = date("Y年n月j日",$item["add_time"]??time());
            return $this->view("",[
                "work"=>$work,
                "item"=>$item,
            ]);
        }
    }

    /**
     * @title 删除
     * @return $this
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new FanjuComics();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
        }

        return $this->success("删除成功");
    }
}