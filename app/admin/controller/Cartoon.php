<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/8/3
 * Time: 18:23
 */

namespace app\admin\controller;
use think\App;
use app\model\FanjuWork;
use app\model\Company;
use app\model\Personnel;
use think\facade\Db;

use app\model\FanjuTvStaff;
use app\model\FanjuTv;
use app\model\TvSeiyuu;
use app\model\FanjuComics;
use app\model\FanjuMovie;
use app\model\FanjuOva;
use app\model\MovieStaff;
use app\model\MovieSeiyuu;
use app\model\OvaSeiyuu;
use app\model\OvaStaff;
//番剧管理
class Cartoon extends Common
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    /**
     * @title 查询
     * @return string
     * @throws \think\db\exception\DbException
     */
    public function index(){

        $keyword = $this->request->get("keyword");
        $model = new FanjuWork();
        if($keyword){
            $model = $model->where("title",'LIKE',"%".$keyword."%")
                ->whereOr("title_jp","LIKE","%".$keyword."%");
        }
        $lists = $model->paginate([
            "query"=>[
                "keyword"=>$keyword
            ]
        ]);

        return $this->view("",[
            "lists"=>$lists
        ]);
    }

    /**
     * @title 获取公司
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getcompany($ids){
        $lists = (new Company())->whereIn("id",$ids)->select();
        $this->assign("companylist",$lists);
    }

    /**
     * @title 添加作品
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function addzp(){
//        $this->getcompany();
        return $this->view("cartoon/post",[
            "item"=>null
        ]);
    }

    /**
     * @title 编辑作品
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function editzp(){

        $id = $this->request->get("id");
        $model = new FanjuWork();
        $item = $model->find($id);
        $this->getcompany($item->company_ids);
        return $this->view("cartoon/post",[
            "item"=>$item
        ]);

    }

    /**
     * @title 提交
     */
    public function post(){
        if($this->request->isPost()){
            $model = new FanjuWork();
            $data = [
                "title"=>$this->request->post("title"),
                "title_jp"=>$this->request->post("title_jp"),
                "author"=>$this->request->post("author"),
                "author_jp"=>$this->request->post("author_jp"),
                "company_ids"=>$this->request->post("company_ids"),
                "work_form"=>$this->request->post("work_form"),
                "desc"=>$this->request->post("desc"),
                "comics_status"=>$this->request->post("comics_status"),
                "cartoon_status"=>$this->request->post("cartoon_status"),
                "comics_all"=>$this->request->post("comics_all"),
                "cartoon_all"=>$this->request->post("cartoon_all"),
                "display"=>$this->request->post("display",0),
            ];
            $check = $this->validate($data,[
                'title|名称(中文)'=>'require',
                'title|名称(日文)'=>'require',
                'author|原作者(中文)'=>'require',
                'author_jp|原作者(日文)'=>'require',
                'company_ids|制作公司'=>'require',
                'work_form|作品形式'=>'require',
            ]);
            if($check){
                return $this->error($check);
            }

            if($data["comics_status"] == 1){
                $data["comics_all"] = 0;
            }
            if($data["cartoon_status"] == 1){
                $data["cartoon_all"] = 0;
            }
            if($data["display"]==1){
                $data["display"]=0;
            }else{
                $data["display"]=1;
            }
            $id = $this->request->post("id");

            if($id){
                $data["company_ids"] = implode(",",$data["company_ids"]);
                $data["work_form"] = implode(",",$data["work_form"]);
                $model->where("id",$id)->update($data);

            }else{
                $model->save($data);
                $id = $model->getLastInsID();
                //查询人名是否存在
                (new Personnel())->add($data["author"],1,$data["author_jp"],0,$id);
            }
            return $this->success("保存成功");
        }
    }

    /**
     * @title 详情
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function details(){
        $id = $this->request->get("id");
        $model = new FanjuWork();

        $item = $model->find($id);
        $work_form = $item["work_form"];
        $sel = $this->request->get("sel",$work_form[0]);
        $search_type = $this->request->get("search_type",null);
        $year = $this->request->get("year",null);
        $staff = $this->request->get("staff",null);

        switch ($sel){
            case 1:

                $tvmodel = new FanjuTv();
                $orderby = "add_time asc,words1 asc,id asc";
                switch ($search_type){
                    case "yzmg":
                        $tvmodel = $tvmodel->where("make_cat",1);
                        break;
                    case "dhzyc":
                        $tvmodel = $tvmodel->where("make_cat",2);
                        break;
                    case "tb":
                        $tvmodel = $tvmodel->where("make_cat",3);
                        break;
                    case "cb":
                        $tvmodel = $tvmodel->where("make_cat",4);
                        break;
                    case "25minute":
                        $tvmodel = $tvmodel->where("tvlenth",1);

                        break;
                    case "teb":
                        $tvmodel = $tvmodel->where("tvlenth",2);
                        break;
                    case "year":
                        $yearArr = explode("-",$year);

                        if(count($yearArr)>=2){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[1]."-01-01 00:00:00"." +1 year -1 day");

                        }else if(count($yearArr)<=1){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[0]."-01-01 23:59:59  +1 year -1 day");

                        }
                        $tvmodel = $tvmodel->where("add_time",'>=',$start)
                            ->where("add_time",'<=',$end);
                        $orderby = "add_time asc";
                        break;
                    case "tdcyr":
                        $staffs = explode(",",$staff);

                        $staffModel = new FanjuTvStaff();
                        foreach($staffs as $val){
                            $staffModel = $staffModel->whereOr("uname",'LIKE','%'.$val.'%')->whereOr("uname_jp",'LIKE','%'.$val.'%');
                        }
                        $tv_ids = $staffModel->column("tv_id");
                        if($tv_ids){
                            $tvmodel = $tvmodel->whereIn("id",$tv_ids);
                        }else{
                            $tvmodel = $tvmodel->where("id",'<',1);
                        }
                        break;
                }
                $lists = $tvmodel
                    ->where("work_id",$id)
                    ->order($orderby)
                    ->append(["staff"])
                    ->paginate([
                        "query"=>[
                            "search_type"=>$search_type,
                            "year"=>$year,
                            "staff"=>$staff,
                            "search"=>$this->request->get("search"),
                            "id"=>$id,
                            "sel"=>$sel,
                        ]
                    ]);
                $this->assign("lists",$lists);
                break;
            case 2:
                $cmodel = new FanjuComics();
                $orderby = "words1 asc,add_time asc,id asc";

                switch ($search_type){
                    case "year":
                        $yearArr = explode("-",$year);

                        if(count($yearArr)>=2){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[1]."-01-01 00:00:00"." +1 year -1 day");

                        }else if(count($yearArr)<=1){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[0]."-01-01 23:59:59  +1 year -1 day");

                        }
                        $cmodel = $cmodel->where("add_time",'>=',$start)
                            ->where("add_time",'<=',$end);
                        $orderby = "add_time asc";
                        break;
                    case "tdcyr":
                        $staffs = explode(",",$staff);
                        foreach ($staffs as $val){
                            $cmodel = $cmodel->whereOr("thumb",'LIKE','%'.$val.'%');
                        }
                        break;
                    case "typec1":
                        $cmodel = $cmodel->where("typec",1);
                        break;
                    case "typec2":
                        $cmodel = $cmodel->where("typec",2);
                        break;
                    case "typec3":
                        $cmodel = $cmodel->where("typec",3);
                        break;
                }
                $lists = $cmodel
                    ->where("work_id",$id)
                    ->order($orderby)
                    ->append(["tv"])
                    ->paginate([
                        "query"=>[
                            "staff"=>$staff,
                            "search_type"=>$search_type,
                            "year"=>$year,
                            "search"=>$this->request->get("search"),
                            "id"=>$id,
                            "sel"=>$sel,
                        ]
                    ]);

                //卷数相同的那啥
                $this->assign("lists",$lists);
                $datas = [];
                $array = $lists->toArray();
                $dytvs=[];
                $cidtotid=[];
                foreach ($array["data"] as $item){

                    if(isset($datas[$item["comics_rolls"]])){
                        array_push($datas[$item["comics_rolls"]],$item);
                    }else{
                        $datas[$item["comics_rolls"]][]=$item;
                    }
                    if($item['tv']){
                        $tvval = $item['tv']->toArray();
                        foreach ($tvval as $sv){
                            $cidtotid[$sv["id"]][]=$item["id"];
                        }
                        foreach ($tvval as $sv){
                            if(isset($dytvs[$sv["id"]])){
                            }else{
                                $dytvs[$sv["id"]]=$sv;
                            }
                        }
                    }
                }
                $arrccc = [];
                $jjarray=[];
                foreach ($cidtotid as $key=>$val){

                    $arrccc[]=[
                        "sid"=>$key,
                        "val"=>$val
                    ];
                }
                foreach ($arrccc as $key=>$val){

                    if(isset($arrccc[$key+1])){
                         $jj = array_intersect($val["val"],$arrccc[$key+1]["val"]);
                         $jjc = count($jj)*0.5;
                         $jjarray[$val["sid"]] = count($cidtotid[$val["sid"]]) - $jjc;
                         $jjarray[$arrccc[$key+1]["sid"]] = count($cidtotid[$arrccc[$key+1]["sid"]]) - $jjc;
                    }
                }
                $this->assign('jjarray',$jjarray);
                $this->assign('dytvs',$dytvs);
                $this->assign("arrayNum",count($array["data"]));
                $this->assign("datas",$datas);
                break;
            case 3:
                $mmodel = new FanjuMovie();
                $orderby = "words1 asc,add_time asc,id asc";

                switch ($search_type){
                    case "year":
                        $yearArr = explode("-",$year);

                        if(count($yearArr)>=2){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[1]."-01-01 00:00:00"." +1 year -1 day");

                        }else if(count($yearArr)<=1){
                            $start =strtotime($yearArr[0]."-01-01 00:00:00");
                            $end = strtotime($yearArr[0]."-01-01 23:59:59  +1 year -1 day");

                        }
                        $mmodel = $mmodel->where("add_time",'>=',$start)
                            ->where("add_time",'<=',$end);

                        $orderby = "add_time asc";

                        break;
                    case "tdcyr":
                        $staffs = explode(",",$staff);

                        $staffModel = new MovieStaff();
                        foreach($staffs as $val){
                            $staffModel = $staffModel->whereOr("username",'LIKE','%'.$val.'%')->whereOr("username_jp",'LIKE','%'.$val.'%');
                        }
                        $movie_id = $staffModel->column("movie_id");
                        if($movie_id){
                            $mmodel = $mmodel->whereIn("id",$movie_id);
                        }else{
                            $mmodel = $mmodel->where("id",'<',1);
                        }
                        break;
                    case "cat1":
                        $mmodel = $mmodel->where("cat",1);
                        break;
                    case "cat2":
                        $mmodel = $mmodel->where("cat",2);
                        break;
                }
                $lists = $mmodel
                    ->where("work_id",$id)
                    ->order($orderby)
                    ->append(["staff"])
                    ->paginate([
                        "query"=>[
                            "staff"=>$staff,
                            "search_type"=>$search_type,
                            "year"=>$year,
                            "search"=>$this->request->get("search"),
                            "id"=>$id,
                            "sel"=>$sel,
                        ]
                    ]);
                $this->assign("lists",$lists);
                break;
            case 4:

                $lists = (new FanjuOva()) ->where("work_id",$id)
                    ->order("words1 asc,add_time asc,id asc")
                    ->append(["staff"])
                    ->paginate([
                        "query"=>[
                            "search_type"=>$search_type,
                            "year"=>$year,
                            "search"=>$this->request->get("search"),
                            "id"=>$id,
                            "sel"=>$sel,
                        ]
                    ]);
                $this->assign("lists",$lists);
                break;
        }
        return $this->view("",[
            "item"=>$item,
            "work_form"=>$work_form,
            "work_form_arr"=>$model->workformnames,
            "sel"=>$sel
        ]);
    }
    /**
     * @title 删除作品
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new FanjuWork();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
            $tvisd = (new FanjuTv())->whereIn("work_id",$id)->column("id");
            $movie_id = (new FanjuMovie())->whereIn("work_id",$id)->column("id");
            $ova_id = (new FanjuOva())->whereIn("work_id",$id)->column("id");
            (new FanjuTv())->whereIn("work_id",$id)->delete();
            (new FanjuComics())->whereIn("work_id",$id)->delete();
            (new FanjuMovie())->whereIn("work_id",$id)->delete();
            (new FanjuOva())->whereIn("work_id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
            $tvisd = (new FanjuTv())->where("work_id",$id)->column("id");
            $movie_id = (new FanjuMovie())->where("work_id",$id)->column("id");
            $ova_id = (new FanjuOva())->where("work_id",$id)->column("id");

            (new FanjuTv())->where("work_id",$id)->delete();
            (new FanjuComics())->where("work_id",$id)->delete();
            (new FanjuMovie())->where("work_id",$id)->delete();

        }
        if($tvisd){
            (new FanjuTvStaff())->whereIn("tv_id",$tvisd)->delete();
            (new TvSeiyuu())->whereIn("tv_id",$tvisd)->delete();
        }
        if($movie_id){
            (new MovieStaff())->whereIn("movie_id",$tvisd)->delete();
            (new MovieSeiyuu())->whereIn("movie_id",$tvisd)->delete();
        }
        if($ova_id){
            (new OvaStaff())->whereIn("movie_id",$tvisd)->delete();
            (new OvaSeiyuu())->whereIn("movie_id",$tvisd)->delete();
        }


        return $this->success("删除成功");
    }

    /**
     * @title 设置连载状态
     * @return $this
     */
    public function setstatus(){
        $ids = $this->request->post("ids");
        $comics_status = $this->request->post("comics_status");
        $cartoon_status = $this->request->post("cartoon_status");
        $comics_all = $this->request->post("comics_all");
        $cartoon_all = $this->request->post("cartoon_all");
        $model = new FanjuWork();
        $model->whereIn("id",$ids)->update([
            "comics_status"=>$comics_status,
            "cartoon_status"=>$cartoon_status,
            "comics_all"=>$comics_all,
            "cartoon_all"=>$cartoon_all,
        ]);
        return $this->success("更新成功");
    }
}