<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/9/22
 * Time: 16:44
 */

namespace app\admin\controller;

use app\model\FanjuMovie;
use app\model\FanjuTvStaff;
use app\model\MovieStaff;
use app\model\MovieSeiyuu;
use app\model\FanjuWork;
use app\model\Personnel;
class Movie extends Common
{
    /**
     * @title 新增条目
     * @return $this
     */
    public function add(){
        $model = new FanjuMovie();
        $work_id = $this->request->post("work_id");
        if(!$work_id){
            return $this->error("作品ID不能为空");
        }
        $words1 = (new FanjuMovie())
            ->where("work_id",$work_id)
            ->order("words1 desc")
            ->count();

        $id = $model->insertGetId([
            "work_id"=>$work_id,
            "words1"=>$words1+1
        ]);
        return $this->success("",[
            "id"=>$id,
            "words1"=>$words1+1
        ]);

    }

    /**
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function post(){
        if($this->request->isPost()){
            $staffModel = new MovieStaff();
            $movieModel = new FanjuMovie();
            $seiYuuModel = new MovieSeiyuu();
            $movi1Info=[
                "words1"=>$this->request->post("words1"),
                "cat"=>$this->request->post("cat"),
                "title"=>$this->request->post("title"),
                "title_jp"=>$this->request->post("title_jp"),
                "add_time"=>$this->request->post("add_time"),
                "time_lenth"=>$this->request->post("time_lenth"),
                "song_man"=>$this->request->post("song_man"),
                "song_title"=>$this->request->post("song_title"),
                "publisher"=>$this->request->post("publisher"),
                "display"=>$this->request->post("display",0),

            ];
            $movi1Info["add_time"] =trim(str_replace(["年","月","日"],'-',$movi1Info["add_time"]),"-");
            $movi1Info["add_time"] = strtotime($movi1Info["add_time"]);
            if($movi1Info["display"]==1){
                $movi1Info["display"]=0;
            }else{
                $movi1Info["display"]=1;
            }
            $id = $this->request->post("id",5);
            $movieModel->where("id",$id)->update($movi1Info);
            $staff = $this->request->post("staff");
            $staffArray = [];
            $item = (new FanjuMovie())->find($id);

            foreach ($staff as $val){
                if($val['username']){
                    $val["movie_id"] = $id;
                    if($val["cat"]==1){
                        (new Personnel())->add($val['username'],1,$val["username_jp"],0,$item["work_id"]);

                    }else{
                        (new Personnel())->add($val['username'],2,$val["username_jp"],0,$item["work_id"]);

                    }
                    array_push($staffArray,$val);
                }

            }

            $staffModel->where("movie_id",$id)->delete();
            $staffModel->insertAll($staffArray);

            $seiyuu = $this->request->post("seiyuu");
            foreach ($seiyuu as &$val){
                $val["movie_id"] = $id;
                if($val["cat"]==1){
                    if($val['seiyuu_zh']){
                        (new Personnel())->add($val['seiyuu_zh'],3,$val["seiyuu_jp"],0,$item["work_id"]);
                    }
                }
            }
            unset($val);
            $seiYuuModel->where("movie_id",$id)->delete();
            $seiYuuModel->insertAll($seiyuu);

            return $this->success("提交成功");
        }else{
            $id = $this->request->get("id");
            $item = (new FanjuMovie())->find($id);
            $work = (new FanjuWork())->find($item["work_id"]);
            $catarray = (new MovieStaff())->catarray;
            $staff = $item->staff;
            $seiYuuModel = new MovieSeiyuu();
            $seiyuulists = $seiYuuModel->where("movie_id",$id)->select();
            return $this->view('',[
                "work"=>$work,
                "item"=>$item,
                "catarray"=>$catarray,
                "staff"=>$staff,
                "seiyuulists"=>$seiyuulists,
            ]);
        }
    }

    /**
     * @title 删除内容
     * @return $this
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new FanjuMovie();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
            (new MovieSeiyuu())->whereIn("movie_id",$id)->delete();
            (new MovieStaff())->whereIn("movie_id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
            (new MovieSeiyuu())->where("movie_id",$id)->delete();
            (new MovieStaff())->where("movie_id",$id)->delete();
        }

        return $this->success("删除成功");
    }
    /**
     * @title 声优查看
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function seiyuu(){
        $id = $this->request->get("id");
        $movie = (new FanjuMovie())->where("id",$id)->find();
        $lists = (new MovieSeiyuu())->where("movie_id",$id)->select();

        return $this->view("",[
            'movie'=>$movie,
            'lists'=>$lists
        ]);

    }

    /**
     * @title 人员获取
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getmans(){
        $words1 = $this->request->get("words1");
        $work_id = $this->request->get("work_id");
        $tvlenth = $this->request->get("tvlenth");
        $movie_id = (new FanjuMovie())
            ->where("words1",$words1)
            ->where("cat",$tvlenth)
            ->where("work_id",$work_id)
            ->value("id");

        if($movie_id){
            $catarray = (new FanjuTvStaff())->catarray;

            $staff = (new MovieStaff())
                ->where("movie_id",$movie_id)
                ->select();
            $staffArray=[];
            foreach ($staff as $key=>$val){
                switch ($val["cat"]){
                    case 2:
                        $staffArray[1][]=$val;
                        break;
                    case 3:
                        $staffArray[2][]=$val;
                        break;
                    case 5:
                        $staffArray[4][]=$val;
                        break;
                    case 7:
                        $staffArray[5][]=$val;
                        break;
                    case 8:
                        $staffArray[6][]=$val;
                        break;
                    case 9:
                        $staffArray[8][]=$val;
                        break;
                    case 10:
                        $staffArray[9][]=$val;
                        break;
                    case 12:
                        $staffArray[10][]=$val;
                        break;
                    case 13:
                        $staffArray[12][]=$val;
                        break;
                    case 14:
                        $staffArray[13][]=$val;
                        break;
                    case 1:
                        $staffArray[14][]=$val;
                        break;
                }

            }
            $seiyuu = (new MovieSeiyuu())
                ->where("movie_id",$movie_id)
                ->select();
            $arr = [];

            foreach ($staffArray as $key=>$val){
                $item = [
                    "id"=>$key,
                    "lists"=>$val
                ];
                $arr[]=$item;
            }
            return $this->success("",[
                "seiyuu"=>$seiyuu,
                "staffArray"=>$arr
            ]);
        }else{
            return $this->success("");
        }

    }
}