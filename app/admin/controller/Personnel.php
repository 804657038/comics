<?php
/**
 * Created by PhpStorm.
 * User: huawei
 * Date: 2021/8/6
 * Time: 19:18
 */

namespace app\admin\controller;
use think\App;
use app\model\Personnel as model;
use app\model\Company;
use think\facade\Db;

class Personnel extends Common
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    /**
     * @return string
     * @throws \think\db\exception\DbException
     */
    public function index(){
        $title = $this->request->get("title");
        $keyword = $this->request->get("keyword");

        $model = new model();
        if($keyword){

            $model = $model
                ->where("(uname LIKE '%".$keyword."%' OR jp_ch LIKE '%".$keyword."%' OR jp_jp LIKE '%".$keyword."%' OR jp_lm LIKE '%".$keyword."%')");
        }
        if($title){
            //搜索作品名称
            $workids = Db::name("fanju_work")
                ->whereOr("title","LIKE","%".$title."%")
                ->whereOr("title_jp","LIKE","%".$title."%")
                ->column("id");

            if($workids){
                $where = [];
                foreach ($workids as $val){
                    $where[]="find_in_set($val,work_id)";
                }
                $model = $model->where(implode(" OR ",$where));
            }else{
                $model = $model->where("id<0");
            }

        }
        $lists = $model->paginate([
            'query'=>[
                "keyword"=>$keyword,
                "title"=>$title
            ]
        ]);

        return $this->view("",[
            "lists"=>$lists,
        ]);
    }
    private function getcompany(){
        $companys = (new Company())->field("id,title_cn")->select();
        $this->assign("companys",$companys);
    }

    public function add(){
        $this->getcompany();
        return $this->view("personnel/post",[
            "item"=>null
        ]);
    }

    /**
     * @title 编辑
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function edit(){
        $this->getcompany();
        $id = $this->request->get("id");
        $model = new model();
        $item = $model->find($id);
        return $this->view("personnel/post",["item"=>$item]);
    }

    public function post(){
        if($this->request->isPost()){
            $data = [
                "uname"=>$this->request->post("uname",null),
                "idcard"=>$this->request->post("idcard",null),
                "birthday"=>$this->request->post("birthday",null),
                "company_id"=>$this->request->post("company_id",null),
                "jp_ch"=>$this->request->post("jp_ch",null),
                "jp_jp"=>$this->request->post("jp_jp",null),
                "jp_lm"=>$this->request->post("jp_lm",null),
            ];
            $check = $this->validate($data,[
                'uname|名称'=>'require',
                'idcard|身份'=>'require',
//                'birthday|生日'=>'require',
                'company_id|所属公司'=>'require',
            ]);
            if($check){
                return $this->error($check);
            }
            $model = new model();
            $id = $this->request->post("id");
            if($id){
                $model->where("id",$id)->save($data);
            }else{
                $model->save($data);
            }
            return $this->success("保存成功");
        }
    }

    /**
     * @title 删除
     * @return $this
     */
    public function del(){
        $id = $this->request->post("id");
        $model = new model();
        if(is_array($id)){
            $model->whereIn("id",$id)->delete();
        }else{
            $model->where("id",$id)->delete();
        }
        return $this->success("删除成功");
    }
}