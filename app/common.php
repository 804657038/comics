<?php
declare (strict_types = 1);
use think\helper\Str;
if(!defined("DS")){
    define("DS",DIRECTORY_SEPARATOR);
}
/**
 * 格式化图片链接
 * @param string $string
 * @return string
 */
function formatImg(string $string,string $domain=""):string{

    if(Str::contains($string,["http","https"])){
        return $string;
    }else{
        $default = config("filesystem")["default"];
        $disks = config("filesystem")["disks"];
        $url = $disks[$default]["url"];
        if(!$domain){
            $domain = request()->domain();
        }
        return $domain.$url.DS.$string;
    };
}

// 应用公共文件
function md5pass(string $pass,string $salt):string{
    return md5(md5($pass).$salt);
}

/**
 * @title 菜单
 * @return array
 */
function menus(){
    $data=[
        [
            "name"=>"番剧管理",
            "path"=>"cartoon/index",
            "childs"=>[
                [
                    "name"=>"录入",
                    "path"=>"cartoon/index",
                ],
                [
                    "name"=>"查询",
                    "path"=>"search/index",
                    "childs"=>[]
                ],
            ]
        ],
        [
            "name"=>"制作公司",
            "path"=>"company/index",
            "childs"=>[]
        ],
        [
            "name"=>"频道管理",
            "path"=>"channel/index",
            "childs"=>[]
        ],
        [
            "name"=>"人员录入",
            "path"=>"personnel/index",
            "childs"=>[]
        ],

//        [
//            "name"=>lang("user"),
//            "path"=>"user/index",
//            "childs"=>[
//                [
//                    "name"=>lang("user.index"),
//                    "path"=>"user/index",
//                ],
//                [
//                    "name"=>lang("user.add"),
//                    "path"=>"user/add",
//                ],
//                [
//                    "name"=>lang("user.roles"),
//                    "path"=>"user/roles",
//                ],
//                [
//                    "name"=>lang("user.log"),
//                    "path"=>"user/log",
//                ]
//            ]
//        ],

    ];
    return $data;
}
/**
 * @title 权限
 * @return array
 */
function purviews(){
    $data = [
        "index"=>[
            "name"=>"后台主页",
            "child"=>[
                "index"=>"主页",
                "index2"=>"数据管理",
                "index3"=>"权限设置",
            ]
        ],
        "cartoon"=>[
            "name"=>"番剧管理",
            "child"=>[
                "index"=>"录入",
                "addzp"=>"新建作品",
                "editzp"=>"编辑作品",
                "postzp"=>"提交作品",
                "del"=>"删除作品",
                "details"=>"作品详情",
                "staff"=>"制作人员名单查看",
                "setstatus"=>"设置状态",

            ]
        ],
        "tv"=>[
            "name"=>"TV管理",
            "child"=>[
                "seiyuu"=>"声优名单查看",
                "del"=>"删除",
                "post"=>"编辑",
                "add"=>"新增条目",
            ]
        ],
        "movie"=>[
            "name"=>"电影管理",
            "child"=>[
                "seiyuu"=>"声优名单查看",
                "del"=>"删除",
                "post"=>"编辑",
                "add"=>"新增条目",
                "getmans"=>"获取制作者和出场者"
            ]
        ],
        "ova"=>[
            "name"=>"OVA管理",
            "child"=>[
                "seiyuu"=>"声优名单查看",
                "del"=>"删除",
                "post"=>"编辑",
                "add"=>"新增条目",
            ]
        ],
        "comics"=>[
            "name"=>"漫画管理",
            "child"=>[
                "del"=>"删除",
                "post"=>"编辑",
                "add"=>"新增条目",
            ]
        ],
        "search"=>[
            "name"=>"查询",
            "child"=>[
                "index"=>"查询",
            ]
        ],
        "company"=>[
            "name"=>"制作公司",
            "child"=>[
                "index"=>"查询",
                "add"=>"添加",
                "edit"=>"编辑",
                "del"=>"删除",
                "post"=>"提交",
            ]
        ],
        "channel"=>[
            "name"=>"频道管理",
            "child"=>[
                "index"=>"查询",
                "add"=>"添加",
                "edit"=>"编辑",
                "del"=>"删除",
                "post"=>"提交",
            ]
        ],
        "personnel"=>[
            "name"=>"人员录入",
            "child"=>[
                "index"=>"查询",
                "add"=>"添加",
                "edit"=>"编辑",
                "del"=>"删除",
                "post"=>"提交",
            ]
        ],

        "user"=>[
            "name"=>lang("user"),
            "child"=>[
                "index"=>lang("user.index"),
                "add"=>lang("user.add"),
                "edit"=>lang("user.edit"),
                "display"=>lang("user.display"),
                "del"=>lang("user.del"),
                "pass"=>lang("user.pass"),
                "roles"=>lang("user.roles"),
                "rolesadd"=>lang("user.rolesadd"),
                "rolesedit"=>lang("user.rolesedit"),
                "rolesdel"=>lang("user.rolesdel"),
//                "log"=>lang("user.log"),
//                "logdel"=>lang("user.logdel"),
            ]
        ],

    ];
    return $data;
}


function lang($name){
    $lang = \think\facade\Cache::get("lang","zh");

    if($lang=="zh"){
        $class = new \lang\Zh();
    }else{
        $class = new \lang\En();
    }
    $data = $class->lang();
    return isset($data[$name])?$data[$name]:$name;
}
/**
 * 防止sql注入
 */

function check_param($value=null) {
    if(!get_magic_quotes_gpc()) {
        // 进行过滤
        $value = addslashes($value);
    }
    $value = str_replace("_", "\_", $value);
    $value = str_replace("%", "\%", $value);
    $value = nl2br($value);
    $value = htmlspecialchars($value);
    return $value;

}
function rawHtml1($vale){
    echo htmlspecialchars_decode(html_entity_decode($vale));
}
function rawHtml($vale){
    $vale = str_replace(["\r","\n"],"<br />",$vale);
    echo htmlspecialchars_decode(htmlspecialchars_decode(htmlspecialchars_decode(html_entity_decode($vale))));
}

/**
 * @title 抽奖
 * @param $proArr
 * @return int|string
 */
function getRand($proArr) { //计算中奖概率
    $rs = ''; //z中奖结果
    $proSum = array_sum($proArr); //概率数组的总概率精度
    //概率数组循环
    foreach ($proArr as $key => $proCur) {
        $randNum = mt_rand(1, (int)$proSum);
        if ($randNum <= $proCur) {
            $rs = $key;
            break;
        } else {
            $proSum -= $proCur;
        }
    }
    unset($proArr);
    return $rs;
}

/**
 * @title curl请求
 * @param string $url 请求的url
 * @param string $method 请求的方式 GET/POST/PUT/DELETE
 * @param string $data 发送的数据
 * @param array $header 请求头
 * @return bool|false|string 返回
 */
function do_curl($url, $method ='GET', $data = '',array $header=[]){
    if ($method == 'POST') {
        //使用crul模拟
        $ch = curl_init();
        //禁用https
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        //允许请求以文件流的形式返回
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, 1);
        if(empty($header)){
            curl_setopt($ch, CURLOPT_HTTPHEADER,array("Content-Type: application/json; charset=utf-8"));
        }else{
            curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        $result = curl_exec($ch); //执行发送


        curl_close($ch);
    }else {
        if (ini_get('allow_fopen_url') == '1') {
            $result = file_get_contents($url);
        }else {
            //使用crul模拟
            $ch = curl_init();
            //允许请求以文件流的形式返回
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

            //禁用https
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

            curl_setopt($ch, CURLOPT_URL, $url);
            $result = curl_exec($ch); //执行发送
            curl_close($ch);
        }
    }
    return $result;
}
function utfToGbk($data){
    return mb_convert_encoding($data,'GBK','utf-8');
}

/**
 * @param $val
 * @return mixed
 */
function formatWeek($val){
    $arr = [
        0=>'日',
        1=>'一',
        2=>'二',
        3=>'三',
        4=>'四',
        5=>'五',
        6=>'六',
    ];
    return $arr[$val];
}
function findNum($str=''){
    $str=trim($str);
    if(empty($str)){return '';}
    $reg='/\d+/';//匹配数字的正则表达式
    preg_match_all($reg,$str,$result);
    return $result[0][0];

}
